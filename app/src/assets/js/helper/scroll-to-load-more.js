import { slice } from 'ramda';

const scrollToLoadMore = ({ data, container, template, cb }) => {
  if (data && container && template) {
    const delayTime = 500;
    const quantityPerLoad = 6;
    const loadTimes = Math.round(data.length / quantityPerLoad);
    const topGap = 80;
    const footer = $('.s-footer');
    const doc = $(document);
    const objHeight = {
      footerHeight: footer.height(),
      documentHeight: doc.height(),
    };
    const state = {
      loaded: 0,
      loading: false,
    };
    const loadResults = slicedData => {
      slicedData.map(item => {
        const html = template(item);
        container.delay(delayTime).queue(function(next) {
          $(this).append(html);
          if (cb) {
            cb();
          }
          objHeight.documentHeight = doc.height();
          state.loading = false;
          next();
        });
      });
    };

    // Init
    loadResults(slice(0, quantityPerLoad, data));

    $(window).resize(function() {
      objHeight.footerHeight = footer.height();
      objHeight.documentHeight = doc.height();
    });

    $(window).scroll(function() {
      const $this = $(this);

      if (
        $this.scrollTop() >=
          objHeight.documentHeight -
            $this.height() -
            objHeight.footerHeight -
            topGap &&
        loadTimes > state.loaded &&
        !state.loading
      ) {
        state.loading = true;
        state.loaded++;
        const start = quantityPerLoad * state.loaded;
        const end = start + quantityPerLoad;
        loadResults(slice(start, end, data));
      }
    });
  }
};

export default scrollToLoadMore;
