/* eslint-disable */
// IE fallback for responsive images while 'object-fit' property is not supported
const ObjectFit = img => {
  if (
    'objectFit' in document.documentElement.style === false &&
    img.length > 0
  ) {
    img.each(function() {
      const self = $(this);
      const imgSrc = self.attr('src');
      const fitType = 'cover';

      self
        .css('visibility', 'hidden')
        .parent()
        .css({
          'background-image': `url(${imgSrc})`,
          'background-repeat': 'no-repeat',
          'background-position': 'center center',
          'background-size': 'cover',
        });
    });
  }
};

export default ObjectFit;
