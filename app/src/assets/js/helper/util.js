const GoTop = el => {
  const button = el.find('.js-go-top');
  button.click(function() {
    $('html, body')
      .stop()
      .animate({ scrollTop: 0 }, 500, 'swing', function() {});
  });
};

const ScrollDown = el => {
  const button = el.find('.js-scroll-down');
  const gap = 64;
  button.click(function() {
    $('html, body')
      .stop()
      .animate(
        { scrollTop: $(window).innerHeight() - gap },
        500,
        'swing',
        function() {}
      );
  });
};

export { GoTop, ScrollDown };
