const Filter = selectEl => {
  if (selectEl.length > 0) {
    selectEl.each(function() {
      const self = $(this);
      const numberOfOptions = self.children('option').length;

      self.addClass('select-hidden');
      self.wrap('<div class="select"></div>');
      self.after('<div class="select-styled"></div>');

      const styledSelect = self.next('div.select-styled');
      styledSelect.html(
        `<span>${self
          .children('option[selected]')
          .eq(0)
          .text()}</span>`
      );

      const list = $('<ul />', {
        class: 'select-options',
      }).insertAfter(styledSelect);

      Array(numberOfOptions)
        .fill()
        .map((_, i) => {
          $('<li />', {
            text: self
              .children('option')
              .eq(i)
              .text(),
            rel: self
              .children('option')
              .eq(i)
              .val(),
            class: `${
              self
                .children('option')
                .eq(i)
                .attr('selected')
                ? 'active'
                : ''
            } ${
              self
                .children('option')
                .eq(i)
                .attr('disabled')
                ? 'disabled'
                : ''
            }`,
          }).appendTo(list);
        });

      const listItems = list.children('li');

      styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled.active')
          .not(this)
          .each(function() {
            $(this)
              .removeClass('active')
              .next('ul.select-options')
              .hide();
          });
        $(this)
          .toggleClass('active')
          .next('ul.select-options')
          .toggle();
      });

      listItems.click(function(e) {
        e.stopPropagation();
        styledSelect
          .html(
            `<span>${$(this)
              .addClass('active')
              .siblings()
              .removeClass('active')
              .end()
              .text()}</span>`
          )
          .removeClass('active');
        self.val($(this).attr('rel'));
        list.hide();
      });
    });

    $(document).click(function() {
      $('div.select-styled').removeClass('active');
      $('.select-options').hide();
    });
  }
};

export default Filter;
