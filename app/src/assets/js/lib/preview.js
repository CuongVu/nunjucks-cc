/* eslint-disable */
$(function() {
  // prettyPrint();
  $('.prettyprinted').removeClass('prettyprinted');

  $('#streetToggle').on('mouseover mousedown touchstart', showStreetLayer);
  $('#buildingToggle').on('mouseover mousedown touchstart', showBuildingLayer);
  $('#streetToggle, #buildingToggle').on('mouseout touchend', resetLayers);
  $(window).on('mouseup', resetLayers);
  var showLayerCss = { alpha: 1, force3D: false, backfaceVisibility: 'hidden' },
    hideLayerCss = { alpha: 0.1, force3D: false, backfaceVisibility: 'hidden' },
    overBgCss = { background: '#CCC' },
    outBgCss = { background: '#EEE' },
    animDuration = 0.5;

  function showStreetLayer(e) {
    TweenMax.to('#streetLayer', animDuration, showLayerCss);
    TweenMax.to('#buildingLayer', animDuration, hideLayerCss);
    TweenMax.to('#streetToggle', animDuration, overBgCss);
    TweenMax.to('#buildingToggle', animDuration, outBgCss);

    return false;
  }
  function showBuildingLayer() {
    TweenMax.to('#streetLayer', animDuration, hideLayerCss);
    TweenMax.to('#buildingLayer', animDuration, showLayerCss);
    TweenMax.to('#streetToggle', animDuration, outBgCss);
    TweenMax.to('#buildingToggle', animDuration, overBgCss);
    return false;
  }

  function resetLayers() {
    TweenMax.to('#streetLayer', animDuration, showLayerCss);
    TweenMax.to('#buildingLayer', animDuration, showLayerCss);
    TweenMax.to('#streetToggle', animDuration, outBgCss);
    TweenMax.to('#buildingToggle', animDuration, outBgCss);
  }
});
