import BrowserSizeController from '../helper/browser-size-controller';
import { GoTop } from '../helper/util';

const HomePage = el => {
  if (el.length > 0) {
    GoTop(el);

    new BrowserSizeController({
      onSmartphone: function() {
        // code for mobile
      },
      onTablet: function() {
        // code for tablet
      },
      onDesktop: function() {
        // code for desktop
      },
    });
  }
};

export default HomePage;
