const $ = require('jquery');
const jQuery = $; // eslint-disable-line

import 'bootstrap';
import 'slick-carousel';
// import 'leaflet';
// import 'jquery-ui/ui/unique-id';
// import 'jquery-ui';
// import './lib/hammer';
// import 'gsap/TweenMax';
// import './lib/zoomer';
// import './lib/preview';

import ObjectFit from './helper/object-fit';
import HomePage from './pages/home';

$(window).on('load', () => {
  ObjectFit($('.object-fit'));
  HomePage($('.p-home'));
});
