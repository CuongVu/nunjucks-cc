const gulp = require('gulp');
const gulpSequence = require('gulp-sequence');

const buildTasks = cb =>
  gulpSequence(
    'clean',
    ['scripts', 'sass', 'images', 'fonts', 'templates'],
    'webpack',
    cb
  );

gulp.task('build', buildTasks);
