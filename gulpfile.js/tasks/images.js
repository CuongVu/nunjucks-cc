const gulp = require('gulp');
const config = require('../config/');
const imagemin = require('gulp-imagemin');
const destPath = `${config.path.build}/assets/img`;
const imagesTask = () => {
  gulp
    .src(`${config.path.assets}/img/**/*`)
    .pipe(
      imagemin({
        interlaced: true,
        progressive: true,
        optimizationLevel: 3,
      })
    )
    .pipe(gulp.dest(destPath));
};

gulp.task('images', imagesTask);
