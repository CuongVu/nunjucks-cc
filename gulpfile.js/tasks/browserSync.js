const gulp = require('gulp');
const config = require('../config/index');
const browserSync = require('browser-sync');
const browserSyncTask = () => {
  const open = process.env.NODE_ENV === 'production' ? false : true;
  const port = parseInt(process.env.PORT, 10) || 3000;
  const host = process.env.HOST || '0.0.0.0';

  browserSync.init({
    port,
    host,
    open,
    server: {
      baseDir: config.path.build,
    },
  });
};

gulp.task('browserSync', browserSyncTask);
